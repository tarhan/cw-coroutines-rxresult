/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.weather

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

private const val STATION = "KDCA"
private const val API_ENDPOINT = "https://api.weather.gov"

class ObservationRemoteDataSource(ok: OkHttpClient) {
  private val retrofit = Retrofit.Builder()
    .client(ok)
    .baseUrl(API_ENDPOINT)
    .addConverterFactory(MoshiConverterFactory.create())
    .build()
  private val api = retrofit.create(ObservationApi::class.java)

  suspend fun getCurrentObservation() = api.getCurrentObservation(STATION)
}

private interface ObservationApi {
  @GET("/stations/{stationId}/observations/current")
  suspend fun getCurrentObservation(@Path("stationId") stationId: String): ObservationResponse
}

data class ObservationResponse(
  val id: String,
  val properties: ObservationProperties
)

data class ObservationProperties(
  val timestamp: String,
  val icon: String,
  val temperature: ObservationValue,
  val windDirection: ObservationValue,
  val windSpeed: ObservationValue,
  val barometricPressure: ObservationValue
)

data class ObservationValue(
  val value: Double?,
  val unitCode: String
)